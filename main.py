
import base64
import collections.abc
import concurrent.futures
import contextlib
import http.client
import io
import json
import urllib.request
import urllib.parse
import os
import pathlib
import re
import sys
import zipfile
import zlib

concurrency = 2
req_timeout = 10
crc_timeout = 300

basedir = pathlib.Path(__file__).absolute().parent
regions = json.loads((basedir / 'regions.json').read_text())
system_mappings = json.loads((basedir / 'system_mappings.json').read_text())
custom_groupings = json.loads((basedir / 'system_extra_groupings.json').read_text())
extensions = json.loads((basedir / 'system_extensions.json').read_text())

cachedir = basedir / 'cache'

re_region = re.compile(
    r'\(((?:{0})(?:,\s*(?:{0}))*)\)'.format('|'.join(map(re.escape, regions))),
    re.IGNORECASE,
    )
re_slug = re.compile(
    rf'''
        \.[a-z]{1-3}$
        |{re_region.pattern}\s*
        |\((?:Beta(\s\d+)?|Rev\s\d+|Sample|Demo)\)\s*
        |\((?:En|Es|Fr|De)\)\s*
        ''',
    re.IGNORECASE | re.VERBOSE,
    )
re_suffix = re.compile(r'\(\d+\)\.[a-z0-9]{2,3}$', re.IGNORECASE)
re_nongame = re.compile(r'^\[[^]]+\]')

system_mapping_urls = {
    k.upper(): [
        system_mappings['$url'].format(name=urllib.parse.quote(x))
        for x in v
        ]
    for k, v in system_mappings.items()
    if not k.startswith('$')
    }
region_priorities = {
    region: index 
    for index, region in enumerate(regions)
    }

thpool = concurrent.futures.ThreadPoolExecutor(concurrency)
pcpool = concurrent.futures.ProcessPoolExecutor(2)
blocksize = 2**20


class naming:

    @classmethod
    def mapping(cls, system: str) -> dict[str, str]:
        system = system.upper()
        return (
            {
                title['searchTerm']: item['group']
                for task in concurrent.futures.as_completed([
                    thpool.submit(jreq, url)
                    for url in system_mapping_urls[system]
                    ], timeout=req_timeout)
                for item in task.result()['variants']
                for key in ('titles', 'compilations')
                for title in item.get(key, ())
                } 
            | {
                x: k
                for k, v in custom_groupings.get(system, {}).items()
                for x in v
                }
            )

    @classmethod
    def group(cls, name, group):
        last = len(regions)
        groups = sorted(
            {
                region
                for name in group
                for region in cls.regions(name)
                }, 
            key=lambda k: region_priorities.get(k, last),
            )
        return f'{name} ({", ".join(groups)})'
    
    @classmethod
    def category(cls, name: str, mapping: collections.abc.Mapping[str, str]) -> str:
        prefix = re_region.split(name, 1)[0].strip()
        return mapping.get(prefix, prefix)
    
    @classmethod
    def regions(cls, name: str) -> list[str]:
        return [
            item.strip() 
            for match in re_region.finditer(name)
            for item in match.group(1).split(',')
            ] or ['World']

    @classmethod
    def categorize(cls, system: str, names: list[str]) -> dict[str, str]:
        groups = collections.defaultdict(list)
        mapping = cls.mapping(system)
        for name in names:
            groups[cls.category(name, mapping)].append(name)
        return {cls.group(k, v): v for k, v in groups.items()}
    

class files:

    @classmethod
    def samecrc32(cls,  *paths: pathlib.Path) -> dict[int, list[pathlib.Path]]:

        if len(paths) == 1:
            return {-1: paths}
        
        samecrc = collections.defaultdict[int, list[pathlib.Path]](list)

        tasks =  [
            pcpool.submit(crc32, pth)
            for pth in paths
            ]
        for task in concurrent.futures.as_completed(tasks, timeout=crc_timeout):
            pth, crc = task.result()
            samecrc[crc].append(pth)

        return dict(samecrc)

    @classmethod
    def samefiles(cls, *paths: pathlib.Path) -> dict[str, list[pathlib.Path]]:
        samenames = collections.defaultdict[str, list[pathlib.Path]](list)
        for pth in paths:
            name = re_suffix.sub('', pth.name).strip()
            samenames[name].append(pth)

        samefiles = collections.defaultdict[str, list[pathlib.Path]](list)
        for name, files in samenames.items():
            samecrc = cls.samecrc32(*files)
        
            if len(samecrc) == 1:
                samefiles[name] = files
                continue
        
            samefiles.update(
                (same[0].with_stem(f'{pth.stem} ({crc})').name, same)
                for crc, same in samecrc.items()
                )

        return dict(samefiles)

    @classmethod
    def collect(
            cls,
            base: os.PathLike,
            system: str | None = None,
            ) -> tuple[list[pathlib.Path], list[pathlib.Path]]:
        base = pathlib.Path(base)
        exts = frozenset(extensions[system or base.name])
        empty_dirs = set()
        roms = []
        for root, dirs, files in base.walk(top_down=False):
            left = len(files)
            for name in files:
                if (path := root / name).suffix in exts:
                    roms.append(path)
                    left -= 1
            if not left and empty_dirs.issuperset(dirs):
                empty_dirs.add(root)
        empty_dirs.discard(base)
        return roms, list(empty_dirs)
    
    @classmethod
    def categorize(
            ls, 
            base: os.PathLike, 
            system: str | None = None,
            ) -> tuple[list[tuple[pathlib.Path, ...]], list[pathlib.Path]]:
        base = pathlib.Path(base)
        system = system or base.name
        roms, empty = files.collect(base, system)

        items = collections.defaultdict(list)
        for pth in roms:
            items[pth.name].append(pth)

        return [
            [base / group, *[
                pth
                for name in names
                for pth in items[name]
                ]]
            for group, names in naming.categorize(system, items).items()
            ], empty
    
    @classmethod
    def move(cls, base: os.PathLike, system: str | None = None):
        groups, empty = cls.categorize(base, system)
        empty = set(empty)
        remove: list[pathlib.Path] = []
        for directory, *files in groups:
            if directory in empty:
                empty.discard(directory)
            else:
                directory.mkdir()
            for name, (first, *others) in cls.samefiles(*files).items():
                first.rename((directory / name).with_suffix(first.suffix))
                remove.extend(others)
        for pth in remove:
            pth.unlink(missing_ok=True)
        for pth in sorted(empty, reverse=True):
            pth.rmdir()


def iter_dat_names(*paths):
    import xml.dom.minidom
    for item in map(pathlib.Path, paths):
        dom = xml.dom.minidom.parseString(item.read_text())
        for game in dom.getElementsByTagName('game'):
            if not re_nongame.match(name := game.getAttribute('name')):
                yield name


def crc32(pth: pathlib.Path) -> tuple[pathlib.Path, int]:
    value = 0

    def chunks(name: str, fd: io.BytesIO):
        if name.endswith('.zip'):
            with zipfile.ZipFile(fd) as zf:
                for zi in zf.infolist():
                    with zf.open(zi) as zo:
                        yield from chunks(zi.filename, zo)
        else:
            while chunk := f.read(blocksize):
                yield chunk

    with pth.open('rb') as f:
        for chunk in chunks(pth.name, f):
            value = zlib.crc32(chunk, value)
    return pth, value


def jreq(url: str) -> dict:
    headers = {'User-Agent': 'ROM dedup'}

    cachedir.mkdir(exist_ok=True, parents=True)

    data = None
    urlb64 = base64.urlsafe_b64encode(url.encode()).decode()
    ignore_missing =  contextlib.suppress(FileNotFoundError)

    cachefile = (cachedir / urlb64).with_suffix('.json')
    with ignore_missing:
        data = cachefile.read_text()

    cacheetag = cachefile.with_suffix(f'.etag')
    with ignore_missing:
        headers['If-None-Match'] = cacheetag.read_text()

    cachedate = cachefile.with_suffix(f'.date')
    with ignore_missing:
        headers['If-Modified-Since'] = cachedate.read_text()

    req = urllib.request.Request(url, headers=headers)
    try:
        with urllib.request.urlopen(req, timeout=req_timeout) as res:
            res: http.client.HTTPResponse
                
            data = res.read()
            if etag := res.headers.get('ETag'):
                cacheetag.write_text(etag)
            
            if date := res.headers.get('Last-Modified'):
                cachedate.write_text(date)

            if etag or date:
                cachefile.write_bytes(data)

    except urllib.request.HTTPError as e:
        if not data:
            raise

        if e.status != 304:
            print(
                f'{req.method} {req.full_url}: status {e.status}', 
                file=sys.stderr,
                )
            
    return json.loads(data)
        

def jprint(data):
    def default(x):
        return repr(x)
    print(json.dumps(data, sort_keys=True, indent=2, default=default))


'''
jprint(categorize('n64', dat_names('Nintendo - Nintendo 64 (BigEndian) (20240527-121959).dat')))
jprint(
    naming.categorize(
        'NES',
        iter_dat_names(
            'Nintendo - Family Computer Disk System (FDS) (20240302-011835).dat',
            'Nintendo - Nintendo Entertainment System (Headered) (20240531-025043).dat',
            )
        ),
    )
jprint(
    files.move('/run/user/1000/gvfs/sftp:host=mister.lan,user=root/media/fat/games/NES')
)


jprint(naming.categorize('n64', iter_dat_names('Nintendo - Nintendo 64 (BigEndian) (20240527-121959).dat')))
'''

files.move('/home/orpheus/Downloads/SNES/')